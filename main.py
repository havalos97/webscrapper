#!/usr/bin/env python

import re
import scrapy
import os
from scrapy.http import Request


class SCRAPER(scrapy.Spider):
    name = 'scraper'
    start_urls = [
        '{URL}',
    ]

    def parse(self, response):
        for td in response.css("td a[href]").extract():
            td = re.sub(r'^.*=\"|\"\>.*$', '', td)
            yield Request(
                url=response.urljoin(td),
                callback=self.save_file
            )

    def save_file(self, response):
        path = response.url.split('/')[-1]
        if not os.path.exists("./files/{PATH}".format(PATH=path)):
            self.logger.info('Saving PDF %s', path)
            fp = open("./files/{PATH}".format(PATH=path), 'wb')
            fp.write(response.body)


def main():
    SCRAPER()


if __name__ == '__main__':
    main()
